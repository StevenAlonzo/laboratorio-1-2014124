package steven.org.lab1_2014124;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText txtpedido=null;
    private Button btnpedido=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtpedido = (EditText) findViewById(R.id.txtPedido);
        btnpedido = (Button) findViewById(R.id.btnpedido);
        btnpedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Click En Pedido",Snackbar.LENGTH_SHORT);
            }
        });

    }
}
